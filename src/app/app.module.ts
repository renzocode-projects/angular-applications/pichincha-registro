import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { SearchPokemonsPipe } from './workflow/pipes/search-pokemons.pipe';
import { TableComponentComponent } from './workflow/components/table-component/table-component.component';
import { RegisterComponentComponent } from './workflow/components/register-component/register-component.component';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsElementsModule } from './modules/shared/atomic-design/atoms/forms-elements/forms-elements.module';
import { ButtonsModule } from './modules/shared/atomic-design/atoms/buttons/buttons.module';
import { RegisterFormService } from './workflow/forms/register-form.service';
import { RegisterImplementsService } from './workflow/implements/register-implements.service';

@NgModule({
  declarations: [
    AppComponent,
    TableComponentComponent,
    RegisterComponentComponent,
    SearchPokemonsPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsElementsModule,
    ButtonsModule
  ],
  providers: [
    RegisterFormService,
    RegisterImplementsService,
    SearchPokemonsPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
