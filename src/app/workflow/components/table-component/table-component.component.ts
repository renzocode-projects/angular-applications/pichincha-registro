import { Component, OnDestroy, OnInit } from '@angular/core';
import { RegisterImplementsService } from '../../implements/register-implements.service';
import { RegisterFormService } from '../../forms/register-form.service';
import { Subscription } from 'rxjs';
import { STATUS_SUCCESSFUL_REPONSES } from 'src/app/modules/shared/parameters/http-status-code.parameter';
import { ICreateFinanceProductTable } from '../../interfaces/responses-interfaces/create-finance-product-response.interface';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { addOneYearToCurrentDate, changeLocalCurrentDate } from 'src/app/modules/shared/helpers/formateDate.helper';

@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.scss']
})
export class TableComponentComponent implements OnInit, OnDestroy {

  public financeProductsList: ICreateFinanceProductTable[] = [];
  private subscriptions: Subscription[] = [];
  public filter: string = '';
  public idValue: string = '';
  constructor(
    public registerForm: RegisterFormService,
    public registerImplements: RegisterImplementsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.registerFormSubs();
    this.getFinanceProducts();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  public registerFormSubs(): void {
    const sub01 = this.registerForm.form.valueChanges
      .subscribe(() => { });
    this.subscriptions.push(sub01);
  }

  public resetRegisterForm(): void {
    this.registerForm.resetRegisterFormControl('fullName');
    this.registerForm.resetRegisterFormControl('description');
    this.registerForm.resetRegisterFormControl('releaseDate');
    this.registerForm.resetRegisterFormControl('id');
    this.registerForm.resetRegisterFormControl('logo');
    this.registerForm.resetRegisterFormControl('reviewDate');
  }

  public getFinanceProducts(): void {
    this.registerImplements.getFinanceProducts$()
      .subscribe((res) => {
        const { data, status } = res;
        if (STATUS_SUCCESSFUL_REPONSES.includes(status)) {
          this.financeProductsList = data?.map((x) => {
            return {
              id: x.id,
              name: x.name,
              description: x.description,
              logo: x.logo,
              date_release: changeLocalCurrentDate(x.date_release),
              date_revision : changeLocalCurrentDate(x.date_revision)
            };
          }) as ICreateFinanceProductTable[];
        }
      })
  }

  public deleteFinanceProduct(id: string): void {
    this.registerImplements.deleteFinanceProductById$(id)
      .subscribe((res)=> {
        const { data, status } = res;
        if (STATUS_SUCCESSFUL_REPONSES.includes(status)) {
          this.getFinanceProducts();
        }
      })
  }

  public validateFinanceProduct(id: string): void {
    this.registerImplements.valiteFinanceProductById$(id)
    .subscribe((res)=> {
      const { data, status } = res;
      if (STATUS_SUCCESSFUL_REPONSES.includes(status)) {
        if (data) {
          this.deleteFinanceProduct(id);
        }
      }
    })
  }

  public goToRegister(): void {
    this.resetRegisterForm();
    this.router.navigate(['/register']);
  }

  public setRegisterForm(financeProductValues: ICreateFinanceProductTable): void {
    const { id, name, date_release, date_revision, description, logo } = financeProductValues;
    this.resetRegisterForm();
    this.registerForm.setRegisterFormControl('id', { value: id } as FormControl);
    this.registerForm.disableRegisterFormControl('id');
    this.registerForm.setRegisterFormControl('fullName', { value: name } as FormControl);
    this.registerForm.setRegisterFormControl('releaseDate', { value: date_release } as FormControl);
    this.registerForm.setRegisterFormControl('reviewDate', { value: addOneYearToCurrentDate(date_revision) } as FormControl);
    this.registerForm.setRegisterFormControl('description', { value: description } as FormControl);
    this.registerForm.setRegisterFormControl('logo', { value: logo } as FormControl);
    this.router.navigate(['/register']);
  }

  public getFinanceProductById(id: string) : ICreateFinanceProductTable {
    return this.financeProductsList.find(element => element.id === id) as ICreateFinanceProductTable;
  }

  public editById(): void {
    this.closeDialog();
    const financeValue = this.getFinanceProductById(this.idValue);
    this.setRegisterForm(financeValue);
  }

  public deleteById(): void {
    this.closeDialog();
    const { id } = this.getFinanceProductById(this.idValue);
    this.deleteFinanceProduct(id);
  }

  public textOutputInput($event: any): void {
    let text = $event.trim().toLowerCase();
    this.filter = text;
  }

  public openDialog($event: any): void {
    this.idValue = $event;
    const myDialog = document.querySelector('#my-dialog') as any;
    myDialog.showModal();
  }

  public closeDialog(): void {
    const myDialog = document.querySelector('#my-dialog') as any;
    myDialog.close();
  }
}
