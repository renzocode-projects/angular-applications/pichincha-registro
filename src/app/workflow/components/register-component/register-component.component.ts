import { Component, OnDestroy, OnInit } from '@angular/core';
import { RegisterFormService } from '../../forms/register-form.service';
import { RegisterImplementsService } from '../../implements/register-implements.service';
import { Subscription, filter } from 'rxjs';
import { FormControl } from '@angular/forms';
import { ICreateFinanceProductRequest } from '../../interfaces/requests-interfaces/create-finance-product-request.interface';
import { STATUS_SUCCESSFUL_REPONSES } from 'src/app/modules/shared/parameters/http-status-code.parameter';
import { addOneYearToCurrentDate, format } from 'src/app/modules/shared/helpers/formateDate.helper';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-component',
  templateUrl: './register-component.component.html',
  styleUrls: ['./register-component.component.scss']
})
export class RegisterComponentComponent implements OnInit, OnDestroy  {

  public isDisabledButton: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(
    public registerForm: RegisterFormService,
    public registerImplement: RegisterImplementsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.disableReviewDateControl();
    this.reviewSubs();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach( subscription => subscription.unsubscribe());
  }

  public disableReviewDateControl(): void {
    this.registerForm.disableRegisterFormControl('reviewDate');
  }

  public setReviewDateByReleaseDate(data: string): void {
    const params = { value: addOneYearToCurrentDate(data)} as FormControl;
    this.registerForm.setRegisterFormControl('reviewDate', params);
  }

  public reviewSubs(): void {
    const subs = this.registerForm.getRegisterFormControl('releaseDate').valueChanges
      .pipe(filter(() => this.registerForm.getRegisterFormControl('releaseDate').valid))
      .subscribe((subs)=> {
        this.setReviewDateByReleaseDate(subs);
      })
    this.subscriptions.push(subs);
  }

  public resetForm(): void {
    this.registerForm.resetRegisterFormControl('fullName');
    this.registerForm.resetRegisterFormControl('description');
    this.registerForm.resetRegisterFormControl('releaseDate');
    this.registerForm.resetRegisterFormControl('id');
    this.registerForm.enableRegisterFormControl('id');
    this.registerForm.resetRegisterFormControl('logo');
    this.registerForm.resetRegisterFormControl('reviewDate');
  }

  public chooseRegisterOrEdit(): boolean {
    return this.registerForm.getRegisterFormControl('id').value && (this.registerForm.getRegisterFormControl('id').status === 'DISABLED');
  }

  public requestRegister(): ICreateFinanceProductRequest {
    const { fullName, description, releaseDate, id, logo, reviewDate } = this.registerForm.formValues;
    return {
        id: id,
        name: fullName,
        description: description,
        date_release: format(releaseDate),
        date_revision: format(reviewDate), 
        logo
      } as ICreateFinanceProductRequest;
  }

  public createFinanceProduct(value: ICreateFinanceProductRequest): void {
    this.registerImplement.createFinanceProduct$(value)
        .subscribe((res) => {
          const { data, status } = res;
          if (STATUS_SUCCESSFUL_REPONSES.includes(status)) {
            this.router.navigate(['/']);
          } else {
          }
      });
  }

  public updateFinanceProduct(value: ICreateFinanceProductRequest): void {
    this.registerImplement.updateFinanceProductById$(value)
        .subscribe((res) => {
          const { data, status } = res;
          if (STATUS_SUCCESSFUL_REPONSES.includes(status)) {
            this.router.navigate(['/']);
          } else {
          }
      });
  }

  public submit(): void {
    if (this.registerForm.form.valid) {
      const isValid = this.chooseRegisterOrEdit();
      const request = this.requestRegister();
      if(isValid) {
        this.updateFinanceProduct(request);
      } else {
        this.createFinanceProduct(request);
      }
    }
  }
}
