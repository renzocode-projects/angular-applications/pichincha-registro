import { Pipe, PipeTransform } from '@angular/core';
import { ICreateFinanceProductTable } from '../interfaces/responses-interfaces/create-finance-product-response.interface';

@Pipe({
  name: 'searchFinanceProducts'
})
export class SearchPokemonsPipe implements PipeTransform {

  transform(values: ICreateFinanceProductTable[], filter: string): ICreateFinanceProductTable[] {
    if (!filter || filter.length === 0) {
      return values;
    }

    if (values.length === 0) {
      return values;
    }

    return values.filter((value: ICreateFinanceProductTable) => {
      const fullNameFound =
        value.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
      const descriptionFound =
        value.description.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
      const dateReleaseFound =
        value.date_release.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
      const dateRevisionFound =
        value.date_revision.toLowerCase().indexOf(filter.toLowerCase()) !== -1;

      if (fullNameFound || descriptionFound || dateReleaseFound || dateRevisionFound) {
        return value;
      }
      return '';
    });
  }

}
