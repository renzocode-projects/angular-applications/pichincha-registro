export interface ITablaData {
  name: string;
  imagenUrl: string;
  attack: string;
  defense: string;
  id: string;
}