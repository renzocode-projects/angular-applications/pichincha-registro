import { TestBed } from '@angular/core/testing';

import { RegisterFormService } from './register-form.service';
import { FormControl, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

describe('RegisterFormService', () => {
  let service: RegisterFormService;

  const parameters = {
    value: 'text'
  } as FormControl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule
      ]
    });
    service = TestBed.inject(RegisterFormService);
  });

  it('Should be fullName Control Enabled', () => {
    service.setRegisterFormControl('fullName', parameters);
    const result = service.getRegisterFormControl('fullName').value;
    const expected = 'text';
    expect(result).toBe(expected);
  });

  it('Should be fullName Control Disabled', () => {
    service.disableRegisterFormControl('fullName');
    const result = service.getRegisterFormControl('fullName').enabled;
    const expected = false;
    expect(result).toBe(expected);
  });

  it('Should be fullName Control Enabled', () => {
    service.enableRegisterFormControl('fullName');
    const result = service.getRegisterFormControl('fullName').enabled;
    const expected = true;
    expect(result).toBe(expected);
  });

  it('Should be fullName Validator Validate', () => {
    service.setRegisterFormValidator('fullName', [Validators.required]);
    const result = service.getRegisterFormControl('fullName').validator?.name;
    const expected = '';
    expect(result).toBe(expected);
  });

  it('Should be fullName Control clear Validates', () => {
    service.clearRegisterFormValidator('fullName');
    const result = service.getRegisterFormControl('fullName').validator;
    const expected = null;
    expect(result).toBe(expected);
  });

  it('Should be fullName Control reset Control', () => {
    service.setRegisterFormControl('fullName', parameters);
    service.resetRegisterFormControl('fullName');
    const result = service.getRegisterFormControl('fullName').value;
    const expected = '';
    expect(result).toBe(expected);
  });

  it('Should be formValues Enabled', () => {
    service.setRegisterFormControl('fullName', parameters);
    const { fullName } = service.formValues;
    const expected = 'text';
    expect(fullName).toBe(expected);
  });
});
