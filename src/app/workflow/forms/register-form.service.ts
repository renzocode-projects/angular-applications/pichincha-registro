import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { dateValidate } from 'src/app/modules/shared/helpers/formateDate.helper';

@Injectable({
  providedIn: 'root'
})
export class RegisterFormService {

  public form: FormGroup = new FormGroup({});
  public idValue = new FormControl({value: '', disabled: false}, [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(10)
  ]);
  public fullNameValue = new FormControl({value: '', disabled: false},  [
    Validators.required,
    Validators.minLength(5),
    Validators.maxLength(100)
  ]);
  public descriptionValue = new FormControl({value: '', disabled: false}, [
    Validators.required,
    Validators.minLength(10),
    Validators.maxLength(200)
  ]);
  public logoValue = new FormControl({value: '', disabled: false}, [
    Validators.required
  ]);
  public dateReleaseValue = new FormControl({value: '', disabled: false}, [
    Validators.required,
    Validators.pattern('^[0-9]{2}/[0-9]{2}/[0-9]{4}$'),
    dateValidate
  ]);
  public reviewValue = new FormControl({value: '', disabled: false}, [
    Validators.required,
    Validators.pattern('^[0-9]{2}/[0-9]{2}/[0-9]{4}$'),
  ]);

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      id: this.idValue,
      description: this.descriptionValue,
      releaseDate: this.dateReleaseValue,
      fullName: this.fullNameValue,
      logo: this.logoValue,
      reviewDate: this.reviewValue
    });
  }

  public setRegisterFormControl(controlName: string, res: FormControl) {
    this.form.controls[controlName].setValue(res.value,
      {
        onlySelf: true,
        emitModelToViewChange: true,
        emitEvent: true
      }
    );
  }

  public getRegisterFormControl(controlName: string): FormControl {
    return this.form.get(controlName) as FormControl;
  }

  public enableRegisterFormControl(controlName: string): void {
    this.getRegisterFormControl(controlName).enable();
  }

  public disableRegisterFormControl(controlName: string): void {
    this.getRegisterFormControl(controlName).disable();
  }

  public setRegisterFormValidator(controlName: string, validator: ValidatorFn | ValidatorFn[]): void {
    this.getRegisterFormControl(controlName).setValidators(validator);
    this.getRegisterFormControl(controlName).updateValueAndValidity({emitEvent: true});
  }

  public clearRegisterFormValidator(controlName: string): void {
    this.getRegisterFormControl(controlName).clearValidators();
    this.getRegisterFormControl(controlName).updateValueAndValidity({emitEvent: true});
  }

  public resetRegisterFormControl(controlName: string): void {
    const newValue = { value: '' }  as FormControl;
    this.setRegisterFormControl(controlName, newValue);
    this.getRegisterFormControl(controlName).markAsPristine();
    this.getRegisterFormControl(controlName).markAsUntouched();
    this.getRegisterFormControl(controlName).updateValueAndValidity({onlySelf: true});
  }

  public get formValues() {
    return {
      fullName: this.fullNameValue.value,
      description: this.descriptionValue.value,
      releaseDate: this.dateReleaseValue.value,
      id: this.idValue.value,
      logo: this.logoValue.value,
      reviewDate: this.reviewValue.value
    };
  }
}
