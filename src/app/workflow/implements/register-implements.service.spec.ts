import { TestBed } from '@angular/core/testing';

import { RegisterImplementsService } from './register-implements.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ICreateFinanceProductReponse } from '../interfaces/responses-interfaces/create-finance-product-response.interface';

describe('RegisterImplementsService', () => {

  const financeProductMock: ICreateFinanceProductReponse[] = [
    {
        id: "1345666",
        name: "carlos alberto",
        description: "cuenta financiera",
        logo: "ddffrfrfr",
        date_release: "2023-10-06T00:00:00.000+00:00",
        date_revision: "2023-10-06T00:00:00.000+00:00"
    }
]

  let service: RegisterImplementsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(RegisterImplementsService);
    httpMock = TestBed.inject(HttpTestingController);
  });


  it('should validate getFinanceProducts$ status 200', () => {
    service.getFinanceProducts$()
    .subscribe((res) => {
      expect(res.status).toEqual(200);
      expect(res.data).toEqual(financeProductMock);
    });
    const req = httpMock.expectOne({
      method: 'GET',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('GET');
    req.flush(financeProductMock, { status: 200, statusText: 'Okay' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate getFinanceProducts$ status 401', () => {
    service.getFinanceProducts$()
    .subscribe((res) => {
      expect(res.status).toEqual(401);
    });
    const req = httpMock.expectOne({
      method: 'GET',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('GET');
    req.flush(financeProductMock, { status: 401, statusText: 'Not Authorization' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate getFinanceProducts$ status 500', () => {
    service.getFinanceProducts$()
    .subscribe((res) => {
      expect(res.status).toEqual(500);
    });
    const req = httpMock.expectOne({
      method: 'GET',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('GET');
    req.flush(financeProductMock, { status: 500, statusText: 'Not Server' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });


  it('should validate createFinanceProduct$ status 200', () => {
    service.createFinanceProduct$()
    .subscribe((res) => {
      expect(res.status).toEqual(200);
      expect(res.data).toEqual(financeProductMock);
    });
    const req = httpMock.expectOne({
      method: 'POST',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('POST');
    req.flush(financeProductMock, { status: 200, statusText: 'Okay' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate createFinanceProduct$ status 401', () => {
    service.createFinanceProduct$()
    .subscribe((res) => {
      expect(res.status).toEqual(401);
    });
    const req = httpMock.expectOne({
      method: 'POST',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('POST');
    req.flush(financeProductMock, { status: 401, statusText: 'Not Authorization' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate createFinanceProduct$ status 500', () => {
    service.createFinanceProduct$()
    .subscribe((res) => {
      expect(res.status).toEqual(500);
    });
    const req = httpMock.expectOne({
      method: 'POST',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('POST');
    req.flush(financeProductMock, { status: 500, statusText: 'Not Server' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate updateFinanceProductById$ status 200', () => {
    service.updateFinanceProductById$()
    .subscribe((res) => {
      expect(res.status).toEqual(200);
      expect(res.data).toEqual(financeProductMock);
    });
    const req = httpMock.expectOne({
      method: 'PUT',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('PUT');
    req.flush(financeProductMock, { status: 200, statusText: 'Okay' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate updateFinanceProductById$ status 401', () => {
    service.updateFinanceProductById$()
    .subscribe((res) => {
      expect(res.status).toEqual(401);
    });
    const req = httpMock.expectOne({
      method: 'PUT',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('PUT');
    req.flush(financeProductMock, { status: 401, statusText: 'Not Authorization' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate updateFinanceProductById$ status 500', () => {
    service.updateFinanceProductById$()
    .subscribe((res) => {
      expect(res.status).toEqual(500);
    });
    const req = httpMock.expectOne({
      method: 'PUT',
      url: service.financeProductURL
    });
    expect(req.request.method).toBe('PUT');
    req.flush(financeProductMock, { status: 500, statusText: 'Not Server' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate deleteFinanceProductById$ status 200', () => {
    const id = '1';
    service.deleteFinanceProductById$(id)
    .subscribe((res) => {
      expect(res.status).toEqual(200);
      expect(res.data).toEqual(financeProductMock);
    });
    const req = httpMock.expectOne({
      method: 'DELETE',
      url: service.financeProductURL  + `?id=${id}`
    });
    expect(req.request.method).toBe('DELETE');
    req.flush(financeProductMock, { status: 200, statusText: 'Okay' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate deleteFinanceProductById$ status 401', () => {
    const id = '1';
    service.deleteFinanceProductById$(id)
    .subscribe((res) => {
      expect(res.status).toEqual(401);
    });
    const req = httpMock.expectOne({
      method: 'DELETE',
      url: service.financeProductURL + `?id=${id}`
    });
    expect(req.request.method).toBe('DELETE');
    req.flush(financeProductMock, { status: 401, statusText: 'Not Authorization' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate deleteFinanceProductById$ status 500', () => {
    const id = '1';
    service.deleteFinanceProductById$(id)
    .subscribe((res) => {
      expect(res.status).toEqual(500);
    });
    const req = httpMock.expectOne({
      method: 'DELETE',
      url: service.financeProductURL + `?id=${id}`
    });
    expect(req.request.method).toBe('DELETE');
    req.flush(financeProductMock, { status: 500, statusText: 'Not Server' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate valiteFinanceProductById$ status 200', () => {
    const id = '1';
    service.valiteFinanceProductById$(id)
    .subscribe((res) => {
      expect(res.status).toEqual(200);
      expect(res.data).toEqual(true);
    });
    const req = httpMock.expectOne({
      method: 'GET',
      url: service.financeProductURL  + `/verification?id=${id}`
    });
    expect(req.request.method).toBe('GET');
    req.flush(true, { status: 200, statusText: 'Okay' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate valiteFinanceProductById$ status 401', () => {
    const id = '1';
    service.valiteFinanceProductById$(id)
    .subscribe((res) => {
      expect(res.status).toEqual(401);
    });
    const req = httpMock.expectOne({
      method: 'GET',
      url: service.financeProductURL + `/verification?id=${id}`
    });
    expect(req.request.method).toBe('GET');
    req.flush(false, { status: 401, statusText: 'Not Authorization' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

  it('should validate valiteFinanceProductById$ status 500', () => {
    const id = '1';
    service.valiteFinanceProductById$(id)
    .subscribe((res) => {
      expect(res.status).toEqual(500);
    });
    const req = httpMock.expectOne({
      method: 'GET',
      url: service.financeProductURL + `/verification?id=${id}`
    });
    expect(req.request.method).toBe('GET');
    req.flush(false, { status: 500, statusText: 'Not Server' });
    httpMock.verify();
    expect(service).toBeTruthy();
  });

});
