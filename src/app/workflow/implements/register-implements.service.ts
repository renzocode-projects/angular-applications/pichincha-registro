import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpRequestService } from 'src/app/modules/core/http-services/http-request.service';
import { IGlobalSuccessfulResponse } from 'src/app/modules/shared/interfaces/generic-response.interface';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ICreateFinanceProductRequest } from '../interfaces/requests-interfaces/create-finance-product-request.interface';
import { ICreateFinanceProductReponse } from '../interfaces/responses-interfaces/create-finance-product-response.interface';

@Injectable({
  providedIn: 'root'
})
export class RegisterImplementsService {
  public readonly financeProductURL = `${environment.apiHeroku.baseUrl}/${environment.apiHeroku.servicePath.baseURLPokemon}`;
  public headers = {};
  constructor(
    public readonly http: HttpRequestService
  ) { 
    this.headers = { 'authorId': '500'};
  }

  public getFinanceProducts$(): Observable<IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>> {
    return this.http.get<ICreateFinanceProductReponse[]>(this.financeProductURL, {}, this.headers)
      .pipe(map((response: HttpResponse<ICreateFinanceProductReponse[]>) => {
        const body = response.body;
        return {
          data: body,
          status: response.status
        } as IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>;
      }))
      .pipe(catchError((response: HttpErrorResponse) => {
        return of({
          data: {},
          status: response.status
        } as  IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>);
      }
    ));
  }

  public createFinanceProduct$(request?: ICreateFinanceProductRequest): Observable<IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>>  {
    return this.http.post<ICreateFinanceProductReponse[]>(this.financeProductURL, {...request}, {...this.headers})
      .pipe(map((response: HttpResponse<ICreateFinanceProductReponse[]>) => {
        const body = response.body;
        return {
          data: body,
          status: response.status
        } as IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>;
      }))
      .pipe(catchError((response: HttpErrorResponse) => {
        return of({
          data: {},
          status: response.status
        } as  IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>);
      }
    ));
  }

  public updateFinanceProductById$(request?: ICreateFinanceProductRequest): Observable<IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>> {
    return this.http.put<ICreateFinanceProductReponse[]>(this.financeProductURL, {...request}, {...this.headers})
      .pipe(map((response: HttpResponse<ICreateFinanceProductReponse[]>) => {
        const body = response.body;
        return {
          data: body,
          status: response.status
        } as IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>;
      }))
      .pipe(catchError((response: HttpErrorResponse) => {
        return of({
          data: {},
          status: response.status
        } as  IGlobalSuccessfulResponse<ICreateFinanceProductReponse[]>);
      }
    ));
  }

  public deleteFinanceProductById$( id?: string): Observable<IGlobalSuccessfulResponse<any[]>> {
    return this.http.delete<any[]>(this.financeProductURL + `?id=${id}`, {}, this.headers)
      .pipe(map((response: HttpResponse<any[]>) => {
        const body = response.body;
        return {
          data: body,
          status: response.status
        } as IGlobalSuccessfulResponse<any[]>;
      }))
      .pipe(catchError((response: HttpErrorResponse) => {
        return of({
          data: {},
          status: response.status
        } as  IGlobalSuccessfulResponse<any[]>);
      }
    ));
  }

  public valiteFinanceProductById$( id?: string ): Observable<IGlobalSuccessfulResponse<boolean>> {
    return this.http.get<boolean>(this.financeProductURL + `/verification?id=${id}`, {}, this.headers)
      .pipe(map((response: HttpResponse<boolean>) => {
        const body = response.body;
        return {
          data: body,
          status: response.status
        } as IGlobalSuccessfulResponse<boolean>;
      }))
      .pipe(catchError((response: HttpErrorResponse) => {
        return of({
          data: {},
          status: response.status
        } as  IGlobalSuccessfulResponse<boolean>);
      }
    ));
  }
}


