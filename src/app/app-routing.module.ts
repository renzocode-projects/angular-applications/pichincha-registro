import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponentComponent } from './workflow/components/table-component/table-component.component';
import { RegisterComponentComponent } from './workflow/components/register-component/register-component.component';

const routes: Routes = [
  {
    path: '',
    component: TableComponentComponent,
    pathMatch: 'full'
  }, 
  {
    path: 'register',
    component: RegisterComponentComponent,
    pathMatch: 'full',
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
