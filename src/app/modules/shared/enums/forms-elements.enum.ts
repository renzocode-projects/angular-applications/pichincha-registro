export enum ETYPES {
  DEFAULT_B = 'default-color',
  SELECT_COLOR_1_B = 'select-color-1',
  INPUT_COLOR_1_A = 'input-color-1',
  BUTTON_COLOR_1_A = 'reset-color-1',
  BUTTON_COLOR_2_A = 'button-color-2'
}