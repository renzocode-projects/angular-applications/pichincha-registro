export enum OBSERVE_TYPE {
  RESPONSE = 'response',
  BODY = 'body',
}
