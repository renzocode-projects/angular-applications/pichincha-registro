export interface IGlobalSuccessfulResponse<IData> {
  status: number;
  data?: IData;
}

export interface IGlobalErrorResponse<IError> {
  status: number;
  error?: IError;
}

export interface IErrorResponseModel {
  errorMessage: string;
}

export interface IGenericParams {
  [ key: string ]: string | string[];
}

export interface IGenericResponse {
  [ key: string ]: string | string[] | number | IGenericResponse;
}

export interface IGenericData {
  [ key: string ]: string | number | boolean | {} | [];
}

