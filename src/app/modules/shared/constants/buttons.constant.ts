import { EPOSITIONS } from "../enums/buttons.enum";
import { ETYPES } from "../enums/forms-elements.enum";

export const PIXELES_REGEXP = /^[0-9]{1,}px$/;
export const B_POSITIONS = [EPOSITIONS.LEFT, EPOSITIONS.RIGTH, EPOSITIONS.CENTER];

export const BUTTONS_TYPES = [
    ETYPES.DEFAULT_B,
    ETYPES.BUTTON_COLOR_1_A,
    ETYPES.BUTTON_COLOR_2_A
];