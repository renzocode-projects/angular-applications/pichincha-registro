import { ETYPES } from "../enums/forms-elements.enum";

export const B_TYPES = [
  ETYPES.DEFAULT_B,
  ETYPES.SELECT_COLOR_1_B,
  ETYPES.INPUT_COLOR_1_A,
];
