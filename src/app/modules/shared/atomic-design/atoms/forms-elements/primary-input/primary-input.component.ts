import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ETYPES } from 'src/app/modules/shared/enums/forms-elements.enum';

@Component({
  selector: 'app-primary-input',
  templateUrl: './primary-input.component.html',
  styleUrls: ['./primary-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PrimaryInputComponent),
      multi: true
    }
  ]
})
export class PrimaryInputComponent implements OnInit {

  @ViewChild('zipEl', { static: true }) zipEl!: ElementRef;
  @Input() label = 'text input label';
  @Input() size = 'm'; // 's', 'm', 'l'
  @Input() marginBottom = '';
  @Input() customClass = '';
  @Input() hasErrorMessage = false;
  @Input() errorMessage = '';
  @Input() width = '';
  @Input() maxLength = 100;
  @Input() placeholder = '';
  @Input() icon = '';
  @Input() type: any = ETYPES.INPUT_COLOR_1_A;
  @Input() isRequired = false;
  @Input() typeOfInput = 'text';
  @Input() isDisabled = true;
  @Input() labelIsDisabled = false;
  @Output() textOutput = new EventEmitter();


  public value = '';
  
  onChange = (_: any) => { };
  onTouch = (_: any) => { };

  constructor(
    private _renderer: Renderer2,
    private _elementRef: ElementRef,
    private cdr:ChangeDetectorRef
  ) {
    
   }

  ngOnInit(): void {
    this.cdr.detectChanges();
  }

  writeValue(value: any): void {
    if (value === '') {
      this._renderer.setProperty(this.zipEl.nativeElement, 'value', value);
    } else {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  public changeInput(target: any): void {
    const value = target.value as HTMLElement;
    this.textOutput.emit(value);
    this.onChange(value);
    this.onTouch(true);
  }
}
