import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimaryInputComponent } from './primary-input/primary-input.component';

@NgModule({
  declarations: [
    PrimaryInputComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PrimaryInputComponent,
  ]
})
export class FormsElementsModule { }
