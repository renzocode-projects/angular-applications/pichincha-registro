import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BUTTONS_TYPES, B_POSITIONS } from 'src/app/modules/shared/constants/buttons.constant';
import { EPOSITIONS } from 'src/app/modules/shared/enums/buttons.enum';
import { ETYPES } from 'src/app/modules/shared/enums/forms-elements.enum';
import { INgStyle } from 'src/app/modules/shared/interfaces/button.interface';

@Component({
  selector: 'app-primary-button',
  templateUrl: './primary-button.component.html',
  styleUrls: ['./primary-button.component.scss']
})
export class PrimaryButtonComponent implements OnInit {

  @Input() type: any = ETYPES.DEFAULT_B;
  @Input() isOutline = false;
  @Input() position: EPOSITIONS = EPOSITIONS.CENTER;
  @Input() isFull = false;
  @Input() isContainerFull = false;
  @Input() isDisabled = false;
  @Input() customWidth = '';
  @Input() iconType = 'close-icon-1';
  public customAttributes: INgStyle = {};

  @Output() onclick = new EventEmitter();

  constructor() { 
  }

  ngOnInit(): void {
    const isValidType = BUTTONS_TYPES.includes(this.type);
    this.type = isValidType ? this.type : ETYPES.DEFAULT_B;
    const isValidPosition = B_POSITIONS.includes(this.position);
    this.position = isValidPosition ? this.position : EPOSITIONS.CENTER;
  }

  public buttonClick(): void {
    if (!this.isDisabled) {
      this.onclick.emit();
    }
  }
}
