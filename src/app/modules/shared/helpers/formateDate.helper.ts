import { AbstractControl } from "@angular/forms";

export const format = (value: any ): string => {
  const [day, month, year] = value.split('/');
  return  year + '-' + month + '-' + day;
}

export function dateValidate(control: AbstractControl): any {
  const controlValue = control.value;
  const [day, month, year] = controlValue.split('/');
  const customDate = new Date(+year, month - 1, +day).setHours(0,0,0,0);
  const today = new Date().setHours(0,0,0,0);
  return customDate >= today ? null: { validate: true }
}

export const changeLocalCurrentDate = (element: string): string => {
  var currentOldDate = new Date(element);
  let currentNewDate = '';
  const dd = String(currentOldDate.getDate() + 1).padStart(2, '0');
  const mm = String(currentOldDate.getMonth() + 1).padStart(2, '0'); //January is 0!
  const yyyy = currentOldDate.getFullYear();
  return currentNewDate =  dd + '/' + mm + '/' + yyyy as string;
};

export const addOneYearToCurrentDate = (element: string): string => {
  let currentNewDate = '';
  const [day, month, year] = element.split('/');
  const newYear = parseInt(year) + 1;
  return currentNewDate =  day + '/' + month + '/' + newYear as string;
}
