export const isObject = (element: any) => {
  const conditions = [
    typeof element === 'object',
    element !== null,
    Object.prototype.toString.call(element) === '[object Object]',
  ];
  // tslint:disable-next-line: variable-name
  const _isObject = conditions.every(condition => condition);
  return _isObject;
};
export const isObjectWithElements = (element: any) => {
  return Object.entries(element).length > 0 && isObject(element);
};
