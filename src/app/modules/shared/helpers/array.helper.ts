export const isArray = (element: any) => {
  const conditions = [
    typeof element === 'object',
    element !== null,
    Object.prototype.toString.call(element) === '[object Array]',
  ];
  // tslint:disable-next-line: variable-name
  const _isArray = conditions.every(condition => condition);
  return _isArray;
}
