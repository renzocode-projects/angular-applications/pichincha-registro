import { Injectable } from '@angular/core';
import { HttpHelperService } from './http-helper.service';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { IGenericData, IGenericParams } from '../../shared/interfaces/generic-response.interface';
import { Observable } from 'rxjs';
import { OBSERVE_TYPE } from '../../shared/enums/http-status-code.enum';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(
    private http: HttpClient,
    private requestHelper: HttpHelperService,
  ) { }

  public get<T>(
    endpoint: string, bodyParams?: IGenericParams, headerParams?: IGenericParams
  ): Observable<HttpResponse<T>> {
    const { transformToHttpParams, transformToHttpHeaders } = this.requestHelper;
    const headers = transformToHttpHeaders(headerParams);
    const params = transformToHttpParams(bodyParams);
    return this.http
      .get<T>(
        endpoint,
        {
          headers,
          observe: OBSERVE_TYPE.RESPONSE,
          params,
          responseType: 'json'
        },
      );
  }

  public post<T>(
    endpoint: string, bodyParams?: IGenericData, headerParams?: IGenericParams
  ): Observable<HttpResponse<T>> {
    const { transformToHttpHeaders } = this.requestHelper;
    const headers = transformToHttpHeaders(headerParams);
    return this.http
      .post<T>(
        endpoint,
        bodyParams,
        {
          headers,
          observe: OBSERVE_TYPE.RESPONSE,
          responseType: 'json'
        }
      );
  }

  public put<T>(
    endpoint: string, bodyParams?: IGenericData, headerParams?: IGenericParams
  ): Observable<HttpResponse<T>> {
    const { transformToHttpHeaders } = this.requestHelper;
    const headers = transformToHttpHeaders(headerParams);

    return this.http
      .put<T>(
        endpoint,
        bodyParams,
        {
          headers,
          observe: OBSERVE_TYPE.RESPONSE,
          responseType: 'json'
        }
      );
  }

  public delete<T>(
    endpoint: string, bodyParams?: IGenericParams, headerParams?: IGenericParams
  ): Observable<HttpResponse<T>> {
    const { transformToHttpParams, transformToHttpHeaders } = this.requestHelper;
    const params = transformToHttpParams(bodyParams);
    const headers = transformToHttpHeaders(headerParams);
    return this.http
      .delete<T>(
        endpoint,
        {
          headers,
          observe: OBSERVE_TYPE.RESPONSE,
          params,
          responseType: 'json'
        },
      );
  }
}
