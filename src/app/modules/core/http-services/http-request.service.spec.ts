import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpRequestService } from './http-request.service';

interface IMockUsers  {
  id: string;
  name: string;
  username: string;
  email: string;
}

const mockUsers: IMockUsers[] = [
  {
    id: '1',
    name: 'Leanne Graham',
    username: 'Bret',
    email: 'Sincere@april.biz'
  }
];

describe('HttpRequestService', () => {
  let service: HttpRequestService;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        HttpRequestService
      ]
    });
    service = TestBed.inject(HttpRequestService);
    controller =  TestBed.inject(HttpTestingController);
  });

  it('should validate GET', () => {
    service.get<IMockUsers[]>('http://localhost:4200/login').subscribe((res) => {
      expect(res.body).toEqual(mockUsers);
    });
    controller.expectOne({
      method: 'GET',
      url: 'http://localhost:4200/login'
    }).flush(mockUsers);
    expect(service).toBeTruthy();
  });

  it('should validate POST', () => {
    service.post<IMockUsers[]>('http://localhost:4200/login', { id: '1' }).subscribe((res) => {
      expect(res.body).toEqual(mockUsers);
    });
    controller.expectOne({
      method: 'POST',
      url: 'http://localhost:4200/login'
    }).flush(mockUsers);
    expect(service).toBeTruthy();
  });

  it('should validate PUT', () => {
    service.put<IMockUsers[]>('http://localhost:4200/login', { id: '1' }).subscribe((res) => {
      expect(res.body).toEqual(mockUsers);
    });
    controller.expectOne({
      method: 'PUT',
      url: 'http://localhost:4200/login'
    }).flush(mockUsers);
    expect(service).toBeTruthy();
  });

  it('should validate DELETE', () => {
    service.delete<IMockUsers[]>('http://localhost:4200/login', { id: '1' }).subscribe((res) => {
      expect(res.body).toEqual(mockUsers);
    });
    controller.expectOne({
      method: 'DELETE',
      url: 'http://localhost:4200/login?id=1'
    }).flush(mockUsers);
    expect(service).toBeTruthy();
  });
});
