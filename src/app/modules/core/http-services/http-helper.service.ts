import { Injectable } from '@angular/core';
import { isObjectWithElements } from '../../shared/helpers/object.helper';
import { IGenericData, IGenericParams } from '../../shared/interfaces/generic-response.interface';
import { HttpHeaders, HttpParams } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class HttpHelperService {

  constructor() { }

  public transformToHttpHeaders(params?: IGenericParams): HttpHeaders {
    if (params) {
      const paramsKeys = isObjectWithElements(params) ? Object.keys(params) : [];
      const httpHeaders = new HttpHeaders(params);
      paramsKeys.forEach(key => {
        httpHeaders.set(key , params[key].toString());
      });
      return httpHeaders;
    }
    return new HttpHeaders();
  }

  public transformToHttpParams(params?: IGenericData): HttpParams {
    if (params) {
      const paramsKeys = isObjectWithElements(params) ? Object.keys(params) : [];
      let httpParams = new HttpParams();
      paramsKeys.forEach(key => {
        httpParams = httpParams.set(key, params[key].toString());
      });
      return httpParams;
    }
    return new HttpParams();
  }


}
